import React, { Component } from "react";

class DisplayData extends Component {
  state = {
    data: ""
  };

  getData = async () => {
    let api = null;
    let results = null;
    api = await fetch("http://localhost:55630/api/values/5");
    results = await api.json();

    console.log(results);
    this.setState({
      data: results
    });
  };

  componentDidMount() {
    this.getData();
  }

  render() {
    return (
      <div>
        <h1>This is a component</h1>
        <h2>{this.state.data}</h2>
      </div>
    );
  }
}

export default DisplayData;
